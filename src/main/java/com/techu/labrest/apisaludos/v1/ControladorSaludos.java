package com.techu.labrest.apisaludos.v1;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

@RestController
@RequestMapping("/api/v1/saludos")
public class ControladorSaludos {

    // GET /api/v1/saludos/ -> LISTA DE SALUDOS.
    // POST /api/v1/saludos/ -> CREA UN NUEVO SALUDO.

    // GET /api/v1/saludos/{id} -> OBTENER UN SALUDOS.
    // DELETE /api/v1/saludos/{id} -> BORRAR UN SALUDO.
    // PUT /api/v1/saludos/{id} -> REEMPLAZAR SALUDO COMPLETO.
    // PATCH /api/v1/saludos/{id} -> OPERACIONES PARA MODIFICAR PARTES DE UN SALUDO.


    //POJO: Plain Old Java Object
    static class Saludo {
        public long id;

        // Atributos públicos Spring los serializa/deserializa
        public String saludoCorto;
        public String saludoLargo;

    }

    private final AtomicLong secuenciaIds = new AtomicLong(0L);
    private final List<Saludo> saludos = new LinkedList<Saludo>();

    @GetMapping("/")
    public List<Saludo> devolverSaludos() {
        return this.saludos;
    }

    @GetMapping("/{id}")
    public Saludo devolverUnSaludo(@PathVariable long id) {
        for(Saludo saludo: this.saludos) {
            if(saludo.id == id)
                return saludo;
        }
        throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }

    @PostMapping("/")
    public Saludo agregarSaludo(@RequestBody Saludo saludo) {
        saludo.id = this.secuenciaIds.incrementAndGet();
        this.saludos.add(saludo);
        return saludo;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void borrarSaludo(@PathVariable long id) {
        for(int i=0; i < this.saludos.size(); ++i) {
            Saludo saludo = this.saludos.get(i);
            if(id == saludo.id) {
                this.saludos.remove(i);
                return;
            }
        }
    }

    @PutMapping("/{id}")
    public void reemplazarSaludo(@PathVariable long id, @RequestBody Saludo saludoNuevo) {
        for(int i = 0; i < this.saludos.size(); i++) {
            Saludo saludo = this.saludos.get(i);
            if(saludo.id == id) {
                saludo.saludoCorto = saludoNuevo.saludoCorto;
                saludo.saludoLargo = saludoNuevo.saludoLargo;
                return;
            }
        }
        throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }

    static class ParcheSaludo {
        public String operacion; // { "borrar-atributo", "modificar-atributos" }
        public String atributo; // cual atributo borrar.
        public Saludo saludo; // los atributos modificados para "modificar-atributos".
    }

    @PatchMapping("/{id}")
    public void modificarSaludo(@PathVariable long id, @RequestBody ParcheSaludo parche) {
        for(int i = 0; i < this.saludos.size(); i++) {
            Saludo saludo = this.saludos.get(i);
            if(saludo.id == id) {
                this.saludos.set(i, emparcharSaludo(saludo, parche));
                return;
            }
        }
        throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }

    private Saludo emparcharSaludo(Saludo saludo, ParcheSaludo parche) {
        if(parche.operacion.trim().equalsIgnoreCase("borrar-atributo")) {
            if("saludoCorto".equalsIgnoreCase(parche.atributo)) {
                saludo.saludoCorto = null;
            }
            if("saludoLargo".equalsIgnoreCase(parche.atributo)) {
                saludo.saludoLargo = null;
            }
        } else if(parche.operacion.trim().equalsIgnoreCase("modificar-atributos")) {
            if(parche.saludo.saludoCorto != null)
                saludo.saludoCorto = parche.saludo.saludoCorto;
            if(parche.saludo.saludoLargo != null)
                saludo.saludoLargo = parche.saludo.saludoLargo;
        }
        return saludo;
    }
}
